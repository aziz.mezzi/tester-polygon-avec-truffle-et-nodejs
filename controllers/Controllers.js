const Web3 = require("web3");
const data = require("../contract.json");

const web3 = new Web3(
  new Web3.providers.HttpProvider("https://rpc-mumbai.maticvigil.com")
);

const TestContract = new web3.eth.Contract(
  data.contractABI,
  data.contractAdresse
);

const addressFrom = "0x0b2f48661Ba8a9b27609E10B524c44f16fC9F509";
const privKey =
  "0xd563ea22d03c4e64984c660a6ec75613a91224549e496a352361e67bc37216aa";

export default controllers = {
  getMessage: (req, res, next) => {
    TestContract.methods
      .message()
      .call(async (err, result) => res.send({ err, result }));
  },

  updateMessage: (req, res, next) => {
    web3.eth.accounts.wallet.add(privKey);

    TestContract.methods
      .update(req.body.newMessage)
      .send({ from: addressFrom, gas: "300000" })
      .then((result) => res.send({ result }));
  },
};
